<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/26/19
 * Time: 7:34 PM
 */

namespace App\Repositories\Frontend;

use App\Models\Region;
use App\Repositories\BaseRepository;

class RegionRepository extends BaseRepository
{
    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return Region::class;
    }

}