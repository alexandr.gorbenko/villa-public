<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 3/2/19
 * Time: 4:09 PM
 */

namespace App\Repositories\Frontend;

use Carbon\Carbon;
use App\Classes\Calendar\Booking;
use App\Models\Calendar\Calendar;
use App\Repositories\BaseRepository;

class CalendarRepository extends BaseRepository
{
    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return Calendar::class;
    }

    public function booking(Booking $booking):bool
    {

        $this->newQuery()->eagerLoad();
        $count = 0;
        if ($booking->isSetAttribute('checkIn') && $booking->isSetAttribute('checkOut')) {
            $count = $this->query->where('villa_id', '=', $booking->getVillaId())
                ->where('from_date', '>=', $booking->getCheckIn()->toDateTimeString())
                    ->where('to_date', '<=', $booking->getCheckOut()->toDateTimeString())->count();
        }

        if ($count == 0) {
            $this->create([
                'villa_id'                      => $booking->getVillaId(),
                'from_date'                     => $booking->getCheckIn()->toDateString(),
                'to_date'                       => $booking->getCheckOut()->toDateTimeString(),
                'client_name'                   => $booking->getClientName(),
                'notifications_email_address'   => $booking->getNotificationsEmailAddress()
            ]);

            return true;
        }

        return false;
    }

}