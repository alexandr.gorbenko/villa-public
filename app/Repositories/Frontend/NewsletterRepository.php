<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 3/25/19
 * Time: 11:54 AM
 */

namespace App\Repositories\Frontend;

use App\Models\Newsletter;
use App\Repositories\BaseRepository;

class NewsletterRepository extends BaseRepository
{
    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return Newsletter::class;
    }

}