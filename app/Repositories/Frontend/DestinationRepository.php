<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/10/19
 * Time: 8:48 PM
 */

namespace App\Repositories\Frontend;

use App\Models\Destination;
use App\Repositories\BaseRepository;

class DestinationRepository extends BaseRepository
{
    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return Destination::class;
    }
}