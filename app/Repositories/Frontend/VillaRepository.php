<?php
/**
 * Created by PhpStorm.
 * User: horbenn
 * Date: 09.02.19
 * Time: 12:03
 */

namespace App\Repositories\Frontend;

use \App\Classes\Villa\Filter;
use App\Models\Villa\Villa;
use App\Models\Calendar\Calendar;
use App\Models\Villa\SeasonPrice;
use App\Repositories\BaseRepository;

class VillaRepository extends BaseRepository
{

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return Villa::class;
    }

    public function byFilter(Filter $filter)
    {
        $this->newQuery()->eagerLoad();

        if ($filter->isSetAttribute('destinationId')) {
            $this->query->where('destination_id', $filter->getDestinationId());
        }

        if ($filter->isSetAttribute('vgNumber')) {
            $this->query->where('vg_number', $filter->getVgNumber());
        }

        if ($filter->isSetAttribute('locationId')) {
            $this->query->where('location_id', $filter->getLocationId());
        }

        if ($filter->isSetAttribute('priceFrom') && $filter->isSetAttribute('priceTo')) {
            $this->query->whereHas('seasonPrice', function ($query) use ($filter) {
                $query->whereBetween((new SeasonPrice())->getTable().'.price', [$filter->getPriceFrom(), $filter->getPriceTo()]);
            });

            if ($filter->isSetAttribute('priceOrderBy')) {
                $this->query->with(['seasonPrice' => function ($query) use ($filter) {
                    $query->orderBy((new SeasonPrice())->getTable().'.price', $filter->getPriceOrderBy());
                }]);
            }
        }

        if ($filter->isSetAttribute('checkIn') && $filter->isSetAttribute('checkOut')) {
            $this->query->whereHas('calendar', function ($query) use($filter) {
                $calendarTable = (new Calendar)->getTable();

                $query->where($calendarTable . '.from_date', '<=', $filter->getCheckIn()->toDateTimeString())
                    ->where($calendarTable . '.to_date', '>=', $filter->getCheckOut()->toDateTimeString());
            });
        }

        $page = \Request::input('page', 1);
        $limit = \Request::input('limit', 25);

        $models = $this->query->paginate($limit, ['*'], 'page', $page);

        $this->unsetClauses();

        return $models;
    }
}