<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 3/25/19
 * Time: 11:41 AM
 */

namespace App\Repositories\Frontend;

use App\Repositories\BaseRepository;
use App\Models\Client;

class ClientRepository extends BaseRepository
{
    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return Client::class;
    }

}