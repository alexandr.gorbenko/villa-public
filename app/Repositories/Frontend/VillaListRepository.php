<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/10/19
 * Time: 6:01 PM
 */

namespace App\Repositories\Frontend;

use App\Models\Villa\VillaList;
use App\Repositories\BaseRepository;

class VillaListRepository extends BaseRepository
{
    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return VillaList::class;
    }
}