<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/10/19
 * Time: 8:46 PM
 */

namespace App\Repositories\Frontend;

use App\Models\Location;
use App\Repositories\BaseRepository;

class LocationRepository extends BaseRepository
{
    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return Location::class;
    }
}