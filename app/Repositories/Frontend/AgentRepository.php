<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 3/5/19
 * Time: 9:13 PM
 */

namespace App\Repositories\Frontend;


use App\Repositories\BaseRepository;
use App\Models\Agent;

class AgentRepository extends BaseRepository
{

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return Agent::class;
    }

}