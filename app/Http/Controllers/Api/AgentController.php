<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Repositories\Frontend\AgentRepository;
use App\Http\Transformers\AgentTransformer;

/**
 * Agent resource representation.
 *
 * @group Agent
 */
class AgentController extends ApiController
{
    protected $agentRepository;

    public function __construct(AgentRepository $agentRepository)
    {
        $this->agentRepository = $agentRepository;
    }

    /**
     * All
     *
     * Get list of all agents with pagination
     *
     * @queryParam page int page num, default = 1
     * @queryParam limit int items per page
     *
     * @responseFile responses/Agent/all.json
     */
    public function all()
    {
        $data = $this->agentRepository->paginate();
        return $this->response()->paginator($data, new AgentTransformer());
    }
    /**
     * By ID
     *
     * Get agent by id
     *
     * @queryParam agent_id int required Agent id
     *
     * @responseFile responses/Agent/item.json
     */
    public function item($id)
    {
        $data = $this->agentRepository->getById($id);
        return $this->response()->item($data, new AgentTransformer());
    }
}
