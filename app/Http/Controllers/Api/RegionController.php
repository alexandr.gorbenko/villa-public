<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Transformers\RegionTransformer;
use App\Repositories\Frontend\RegionRepository;

/**
 * Region resource representation.
 *
 * @group Region
 */
class RegionController extends ApiController
{
    protected $regionRepository;

    public function __construct(RegionRepository $regionRepository)
    {
        $this->regionRepository = $regionRepository;
    }

    /**
     * All
     *
     * Get list of all region
     *
     * @queryParam include string including additional info from from list [location, destination]. Example: ?include=location
     *
     * @responseFile responses/Region/region.json
     */
    public function region()
    {
        $data = $this->regionRepository->all();
        return $this->response()->collection($data, new RegionTransformer());
    }
}
