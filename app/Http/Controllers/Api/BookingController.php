<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Classes\Calendar\Booking;
use App\Repositories\Frontend\CalendarRepository;

/**
 * Booking resource representation.
 *
 * @group Booking
 */
class BookingController extends ApiController
{
    protected $calendarRepository;

    public function __construct(CalendarRepository $calendarRepository)
    {
        $this->calendarRepository = $calendarRepository;
    }

    /**
     * Booking
     *
     * Send check in and check out for Booking now
     * Response status_code = 200 or 500 and message "Can check out" or "Can`t check out"
     *
     * @queryParam villa-id int required Villa id
     * @queryParam check-in string required Check in date
     * @queryParam check-out string required Check out date
     * @queryParam client-name string required Client name
     * @queryParam notifications-email-address string required Notifications email address
     *
     * @responseFile responses/booking.json
     */
    public function booking(Request $request)
    {
        $this->validate($request, [
            'villa-id'                      => ['required', 'numeric'],
            'check-in'                      => ['required', 'date'],
            'check-out'                     => ['required', 'date'],
            'client-name'                   => ['required', 'string'],
            'notifications-email-address'   => ['required', 'email'],
        ]);

        $booking = (new Booking())
            ->setInt('villaId', $request['villa-id'])
            ->setDate('checkIn', $request['check-in'])
            ->setDate('checkOut', $request['check-out'])
            ->setString('clientName', $request['client-name'])
            ->setString('notificationsEmailAddress', $request['notifications-email-address']);

        $canCheckout = $this->calendarRepository->booking($booking);
        $response = ($canCheckout) ? ['message' => 'Can check out', 'status_code' => 200] : ['message' => 'Can`t check out', 'status_code' => 500];

        return response()->json($response);
    }
}
