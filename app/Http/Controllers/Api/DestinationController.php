<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Transformers\DestinationTransformer;
use App\Repositories\Frontend\DestinationRepository;

/**
 * Destination resource representation.
 *
 * @group Destination
 */
class DestinationController extends ApiController
{
    protected $destinationRepository;

    public function __construct(DestinationRepository $destinationRepository)
    {
        $this->destinationRepository = $destinationRepository;
    }

    /**
     * All
     *
     * Get list of all destination
     *
     * @queryParam include string including additional info from from list [location]. Example: ?include=destination
     *
     * @responseFile responses/Destination/destination.json
     */
    public function destination()
    {
        $data = $this->destinationRepository->all();
        return $this->response()->collection($data, new DestinationTransformer());
    }
}
