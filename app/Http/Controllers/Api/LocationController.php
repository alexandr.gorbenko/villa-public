<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Transformers\LocationTransformer;
use App\Repositories\Frontend\LocationRepository;

/**
 * Location resource representation.
 *
 * @group Location
 */
class LocationController extends ApiController
{
    protected $locationRepository;

    public function __construct(LocationRepository $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    /**
     * All
     *
     * Get list of all location
     *
     * @queryParam include string including additional info from from list [destination]. Example: ?include=destination
     *
     * @responseFile responses/Location/location.json
     */
    public function location()
    {
        $data = $this->locationRepository->all();
        return $this->response()->collection($data, new LocationTransformer());
    }
}
