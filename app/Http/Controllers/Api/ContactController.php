<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Repositories\Frontend\ClientRepository;

/**
 * Contact resource representation.
 *
 * @group Contact
 */
class ContactController extends ApiController
{
    protected $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * Contact form
     *
     * Send contact form data
     *
     * @queryParam first-name string required Client name
     * @queryParam last-name string required Client name
     * @queryParam phone string required Client phone
     * @queryParam email string required Client email
     * @queryParam message string Message
     *
     * @responseFile responses/contact.json
     */
    public function contact(Request $request)
    {
        $this->validate($request, [
            'first-name'    => ['required', 'string'],
            'last-name'     => ['required', 'string'],
            'phone'         => ['required', 'string'],
            'email'         => ['required', 'email'],
            'message'       => ['string'],
        ]);

        $this->clientRepository->create([
            'firstname' => $request['first-name'],
            'lastname' => $request['last-name'],
            'email_1' => $request['email'],
            'phone_home' => $request['phone'],
            'source' => 1,
            'notes' => $request['message'],
        ]);

        return response()->json([
            'message' => 'Created',
            'status_code' => 200
        ]);
    }
}
