<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Transformers\VillaListTransformer;
use App\Repositories\Frontend\VillaListRepository;

/**
 * VillaList resource representation.
 *
 * @group VillaList
 *
 */
class VillaListController extends ApiController
{
    protected $villaListRepository;

    public function __construct(VillaListRepository $villaListRepository)
    {
        $this->villaListRepository = $villaListRepository;
    }

    /**
     * By ID
     *
     * Get the villa-list by id
     *
     * @bodyParam id int VillaList ID
     *
     * @queryParam include string including additional info from from list [villa]. Example: ?include=villa
     *
     * @responseFile responses/VillaList/item.json
     *
     */
    public function item($id)
    {
        $data = $this->villaListRepository->getById($id);
        return $this->response()->item($data, new VillaListTransformer());
    }

    /**
     * All
     *
     * Get a list of villa-list with pagination
     *
     * @queryParam page int page num, default = 1
     * @queryParam limit int items per page
     * @queryParam include string including additional info from from list [villa]. Example: ?include=villa
     *
     * @responseFile responses/VillaList/all.json
     *
     */
    public function all()
    {
        $data = $this->villaListRepository->paginate();
        return $this->response()->paginator($data, new VillaListTransformer());
    }
}
