<?php

namespace App\Http\Controllers\Api;

use Validator;
use App\Classes\Villa\Filter;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Transformers\VillaTransformer;
use App\Repositories\Frontend\VillaRepository;

/**
 * Villa resource representation.
 *
 * @group Villa
 *
 */
class VillaController extends ApiController
{
    protected $villaRepository;

    public function __construct(VillaRepository $villaRepository)
    {
        $this->villaRepository = $villaRepository;
    }


    /**
     *
     * All
     *
     * Get list of all villas with pagination
     *
     * @queryParam page int page num, default = 1
     * @queryParam limit int items per page
     * @queryParam include string including additional info from from list [location, rates, agent, villaList, season, seasonPrice, recommendation, image, videos, testimonial, calendar, review, amenities]. Example: ?include=villaList,season,seasonPrice
     *
     * @responseFile responses/Villa/all.json
     *
     */
    public function all() {
        $data = $this->villaRepository->paginate();
        return $this->response()->paginator($data, new VillaTransformer());
    }

    /**
     * Filtered
     *
     * Get list of villas with filter params
     *
     * @queryParam page int page num, default = 1
     * @queryParam limit int items per page
     * @queryParam include string including additional info from from list [location, rates, agent, villaList, season, seasonPrice, recommendation, image, videos, testimonial, calendar, review, amenities]. Example: ?include=villaList,season,seasonPrice
     * @queryParam check-in date
     * @queryParam check-out date
     * @queryParam destination-id int
     * @queryParam location-id int
     * @queryParam price-from int
     * @queryParam price-to int
     * @queryParam number string
     * @queryParam sort-price string values[high, low]
     *
     * @responseFile responses/Villa/all.json
     */
    public function filter(Request $request)
    {
        $this->validate($request, [
            'check-in'          => ['date'],
            'check-out'         => ['date'],
            'destination-id'    => ['numeric'],
            'location-id'       => ['numeric'],
            'price-from'        => ['numeric'],
            'price-to'          => ['numeric'],
            'sort-price'        => ['string'],
            'number'            => ['string'],
        ]);

        $filter = (new Filter())
            ->setDate('checkIn', $request['check-in'])
            ->setDate('checkOut', $request['check-out'])
            ->setInt('destinationId', $request['destination-id'])
            ->setInt('locationId', $request['location-id'])
            ->setInt('priceFrom', $request['price-from'])
            ->setInt('priceTo', $request['price-to'])
            ->setString('vgNumber', $request['number']);

        if (isset($request['sort-price'])) {
            switch ($request['sort-price']) {
                case 'high':
                    $filter->setString('priceSortBy', 'desc');
                    break;
                case 'low':
                    $filter->setString('priceSortBy', 'asc');
                    break;
            }
        }

        $data = $this->villaRepository->byFilter($filter);
        return $this->response()->paginator($data, new VillaTransformer());
    }

    /**
     * By ID
     *
     * Get the villa by id
     *
     * @bodyParam id int Villa ID
     *
     * @queryParam include string including additional info from from list [location, rates, agent, villaList, season, seasonPrice, recommendation, image, videos, testimonial, calendar, review, amenities]. Example: ?include=villaList,season,seasonPrice
     *
     * @responseFile responses/Villa/item.json
     *
     */
    public function item($id)
    {
        $data = $this->villaRepository->getById($id);
        return $this->response()->item($data, new VillaTransformer());
    }

}
