<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Repositories\Frontend\NewsletterRepository;

/**
 * Newsletter resource representation.
 *
 * @group Newsletter
 */
class NewsletterController extends ApiController
{
    protected $newsletterRepository;

    public function __construct(NewsletterRepository $newsletterRepository)
    {
        $this->newsletterRepository = $newsletterRepository;
    }

    /**
     * Newsletter form
     *
     * Send Newsletter form data
     *
     * @queryParam first-name string required Client first name
     * @queryParam email string required Client email phone
     */
    public function newsletter(Request $request)
    {
        $this->validate($request, [
            'first-name'    => ['required', 'string'],
            'email'         => ['required', 'email'],
        ]);

        $this->newsletterRepository->create([
            'first_name'    => $request['first-name'],
            'email'         => $request['email'],
            'opt_out'       => 0
        ]);

        return response()->json([
            'message' => 'Created',
            'status_code' => 200
        ]);
    }

}