<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/26/19
 * Time: 8:01 PM
 */

namespace App\Http\Transformers;

use App\Models\Region;

class RegionTransformer extends BaseTransformer
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'location',
        'destination'
    ];
    /**
     * Transform a response with a transformer.
     *
     * @param Region $region
     * @return array
     */
    public function transform(Region $region)
    {
        return [
            'region_id' => (int) $region->region_id,
            'name' => $region->name,
            'num' => $region->num,
            'buddy_region' => $region->buddy_region,
            'change_state' => $region->change_state,
            'weights' => $region->weights,
            'cs_weight' => $region->cs_weight,
            'title' => $region->title,
            'description' => $region->description,
            'long_description' => $region->long_description,
            'transfer_info' => $region->transfer_info,
            'local_contact' => $region->local_contact,
            'activities' => $region->activities,
            'head_title' => $region->head_title,
            'head_code' => $region->head_code,
            'meta_description' => $region->meta_description,
            'location_id' => (int) $region->location_id,
            'agent_id' => (int) $region->agent_id,
            'destination_id' => (int) $region->destination_id,
            'image' => $region->image,
            'general_guide' => $region->general_guide,
            'slug' => $region->slug,
        ];
    }

    /**
     * Include Location
     *
     * @param Region $region
     * @return \League\Fractal\Resource\Item
     */
    public function includeLocation(Region $region)
    {
        return ($region->location) ? $this->item($region->location, new LocationTransformer) : null;
    }

    /**
     * Include Destination
     *
     * @param Region $region
     * @return \League\Fractal\Resource\Item
     */
    public function includeDestination(Region $region)
    {
        return ($region->destination) ? $this->item($region->destination, new DestinationTransformer) : null;
    }



}