<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/12/19
 * Time: 8:40 PM
 */

namespace App\Http\Transformers;

use App\Models\Image;

class ImageTransformer extends BaseTransformer
{
    /**
     * Transform a response with a transformer.
     *
     * @param Image $image
     * @return array
     */
    public function transform(Image $image)
    {
        return [
            'content' => $image->content,
            'caption' => $image->caption,
            'slideshow_homepage' => $image->slideshow_homepage,
            'orientation' => $image->orientation,
            'created_at' => $image->created_at,
            'updated_at' => $image->updated_at,
            'image_id' => $image->image_id,
            'villa_id' => (int) $image->villa_id,
            'priority' => $image->priority,
            'full' => $image->full,
            'main' => $image->main,
            'thumb' => $image->thumb,
        ];
    }

}