<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/12/19
 * Time: 9:31 PM
 */

namespace App\Http\Transformers;

use App\Models\Location;

class LocationTransformer extends BaseTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'destination'
    ];

    /**
     * Transform a response with a transformer.
     *
     * @param Location $location
     * @return array
     */
    public function transform(Location $location)
    {
        return [
            'location_url' => $location->location_url,
            'location_id' => (int) $location->location_id,
            'destination_id' => (int) $location->destination_id,
            'name' => $location->name,
            'title' => $location->title,
            'description' => $location->description,
            'long_description' => $location->long_description,
            'transfer_info' => $location->transfer_info,
            'local_contact' => $location->local_contact,
            'agent_id' => (int) $location->agent_id,
            'activities' => $location->activities,
            'head_title' => $location->head_title,
            'head_code' => $location->head_code,
            'meta_description' => $location->meta_description,
            'image' => $location->image,
            'general_guide' => $location->general_guide,
            'slug' => $location->slug,
        ];
    }

    /**
     * Include destination
     *
     * @param Location $location
     * @return \League\Fractal\Resource\Item
     */
    public function includeDestination(Location $location)
    {
        return $this->item($location->destination, new DestinationTransformer());
    }

}