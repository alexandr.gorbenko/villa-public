<?php
/**
 * Created by PhpStorm.
 * User: auto
 * Date: 10.03.19
 * Time: 11:21
 */

namespace App\Http\Transformers;

use App\Models\Villa\Vm\Rates;

class RatesTransformer extends BaseTransformer
{



    /**
     * Transform a response with a transformer.
     *
     * @param Rates $rates
     * @return array
     */
    public function transform(Rates $rates)
    {
        return [
            'villa_id' => $rates->villa_id ,
            'vg_number' => $rates->vg_number,
            'villa_name' => $rates->villa_name,
            'from_date' => $rates->from_date,
            'to_date' => $rates->to_date,
            'season_type_id' => $rates->season_type_id,
            'season_name' => $rates->season_name,
            'price' => $rates->price,
            'nr_of_rooms' => $rates->nr_of_rooms,
            'min_nr_days' => $rates->min_nr_days,
            'master_included' => $rates->master_included,
        ];
    }

}