<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 3/5/19
 * Time: 9:16 PM
 */

namespace App\Http\Transformers;

use App\Models\Agent;

class AgentTransformer extends BaseTransformer
{

    /**
     * Transform a response with a transformer.
     *
     * @param Agent $agent
     * @return array
     */
    public function transform(Agent $agent)
    {
        return [
            'agent_id' => (int) $agent->agent_id,
            'last_enquiry_dashboard' => $agent->last_enquiry_dashboard,
            'comm1_agent' => $agent->comm1_agent,
            'comm1_percent' => $agent->comm1_percent,
            'comm1_percent_automated' => $agent->comm1_percent_automated,
            'agent_comm1_boost' => $agent->agent_comm1_boost,
            'comm2_agent' => $agent->comm2_agent,
            'comm2_percent' => $agent->comm2_percent,
            'comm2_percent_automated' => $agent->comm2_percent_automated,
            'comm3_agent' => $agent->comm3_agent,
            'comm3_percent' => $agent->comm3_percent,
            'comm4_agent' => $agent->comm4_agent,
            'comm3_percent_automated' => $agent->comm3_percent_automated,
            'comm4_percent' => $agent->comm4_percent,
            'comm4_percent_automated' => $agent->comm4_percent_automated,
            'active' => $agent->active,
            'can_forward' => $agent->can_forward,
            'user_id' => (int) $agent->user_id,
            'created_at' => $agent->created_at,
            'updated_at' => $agent->updated_at,
        ];
    }

}