<?php

namespace App\Http\Transformers;


use App\Models\Villa\Villa;

class VillaTransformer extends BaseTransformer
{
    /**
     * List of resources default include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //'amenities'
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'villaList',
        'season',
        'seasonPrice',
        'recommendation',
        'image',
        'videos',
        'testimonial',
        'calendar',
        'review',
        'agent',
        'rates',
        'location'
    ];

    /**
     * Transform a response with a transformer.
     *
     * @param Villa $villa
     * @return array
     */
    public function transform(Villa $villa)
    {
        return [
            'villa_id' => $villa->villa_id,
            'advance_days' => $villa->advance_days,
            'booknow' => $villa->booknow,
            'deposit_percentage' => $villa->deposit_percentage,
            'security_deposit' => $villa->security_deposit,
            'commission_percentage' => $villa->commission_percentage,
            'tax_percentage' => $villa->tax_percentage,
            'peak_full_only' => $villa->peak_full_only,
            't_availability_exclusive' => $villa->t_availability_exclusive,
            'oceanfront' => $villa->oceanfront,
            'display_online' => $villa->display_online,
            'is_exclusive' => $villa->is_exclusive,
            'is_priority' => $villa->is_priority,
            't_platinum' => $villa->t_platinum,
            't_absolute_beachfront' => $villa->t_absolute_beachfront,
            't_wedding' => $villa->t_wedding,
            't_premium' => $villa->t_premium,
            't_corporate_retreats' => $villa->t_corporate_retreats,
            'platinum_name' => $villa->platinum_name,
            'latitude' => $villa->latitude,
            'longitude' => $villa->longitude,
            'display_map' => $villa->display_map,
            't_availability' => $villa->t_availability,
            't_seasons' => $villa->t_seasons,
            'platinum_description' => $villa->platinum_description,
            'platinum_testimonial' => $villa->platinum_testimonial,
            'platinum_image' => $villa->platinum_image,
            'platinum_rating' => $villa->platinum_rating,
            'agent_id' => (int) $villa->agent_id,
            'agent_assigned_id' => (int) $villa->agent_assigned_id,
            'summary' => $villa->summary,
            'description' => $villa->description,
            'exclusive_summary' => $villa->exclusive_summary,
            'exclusive_description' => $villa->exclusive_description,
            'rating' => $villa->rating,
            'video' => $villa->video,
            'villa_address' => $villa->villa_address,
            'exclusive_name' => $villa->exclusive_name,
            'villa_phone' => $villa->villa_phone,
            'villa_fax' => $villa->villa_fax,
            'manager_name' => $villa->manager_name,
            'manager_phone' => $villa->manager_phone,
            'manager_email' => $villa->manager_email,
            'for_sale' => $villa->for_sale,
            'for_sale_price' => $villa->for_sale_price,
            'for_sale_summary' => $villa->for_sale_summary,
            'for_sale_description' => $villa->for_sale_description,
            'arrival_information' => $villa->arrival_information,
            'website' => $villa->website,
            'transfer_info' => $villa->transfer_info,
            'local_contact' => $villa->local_contact,
            'activities' => $villa->activities,
            'general_guide' => $villa->general_guide,
            'title' => $villa->title,
            'head_title' => $villa->head_title,
            'head_code' => $villa->head_code,
            'meta_description' => $villa->meta_description,
            'created_at' => $villa->created_at,
            'updated_at' => $villa->updated_at,
            'updated_by' => $villa->updated_by,
            'currency_code' => $villa->currency_code,
            't_seasons_exclusive' => $villa->t_seasons_exclusive,
            'name' => $villa->name,
            'owner_id' => (int) $villa->owner_id,
            'vg_number' => $villa->vg_number,
            'order_number' => $villa->order_number,
            'destination_id' => (int) $villa->destination_id,
            'location_id' => (int) $villa->location_id,
            'region_id' => (int) $villa->region_id,
            'external_id' => (int) $villa->external_id,
            'external_service' => $villa->external_service,
            'price_updated_at' => $villa->price_updated_at,
            'price_updated_by' => $villa->price_updated_by,
            'booking_updated_at' => $villa->booking_updated_at,
            'booking_updated_by' => $villa->booking_updated_by,
            'sleeps' => $villa->sleeps,
            'sleeps_max' => $villa->sleeps_max,
            'beds' => $villa->beds,
            'baths' => $villa->baths,
            't_for_sale' => $villa->t_for_sale,
            'gm_country' => $villa->gm_country,
            'gm_city' => $villa->gm_city,
            'gm_address' => $villa->gm_address,
            'gm_number' => $villa->gm_number,
            'address' => $villa->address,
            'phone' => $villa->phone,
            'fax' => $villa->fax,
            'm_name' => $villa->m_name,
            'm_phone' => $villa->m_phone,
            'm_email' => $villa->m_email,
            'homeaway_headline' => $villa->homeaway_headline,
            'coords' => $villa->coords,
            'contact_person_first_name' => $villa->contact_person_first_name,
            'contact_person_last_name' => $villa->contact_person_last_name,
            'contact_person_email' => $villa->contact_person_email,
            'contact_person_mobile_phone' => $villa->contact_person_mobile_phone,
            'number_' => $villa->number_,
            'room_number' => $villa->room_number,
            'owner' => $villa->owner,
            'has_bookings' => $villa->has_bookings,
            'status' => $villa->status,
            'number_of_reviews' => $villa->number_of_reviews,
            'amenities' => [
                'data' => [
                    'oceanview' => $villa->oceanview,
                    'pool' => $villa->pool,
                    'ac' => $villa->ac,
                    'maid' => $villa->maid,
                    'chef' => $villa->chef,
                    'broadband' => $villa->broadband,
                    'tennis' => $villa->tennis,
                    'daily_breakfast' => $villa->daily_breakfast,
                    'car_and_driver' => $villa->car_and_driver,
                ]
            ]
        ];
    }

    /**
     * Include VillaList
     *
     * @param Villa $villa
     * @return \League\Fractal\Resource\Collection
     */
    public function includeVillaList(Villa $villa)
    {
        $villaList = $villa->villaList;
        return $this->collection($villaList, new VillaListTransformer) ?? null;
    }

    /**
     * Include season
     *
     * @param Villa $villa
     * @return \League\Fractal\Resource\Collection
     */
    public function includeSeason(Villa $villa)
    {
        return $this->collection($villa->season, new SeasonTransformer) ?? null;
    }

    /**
     * Include seasonPrice
     *
     * @param Villa $villa
     * @return \League\Fractal\Resource\Collection
     */
    public function includeSeasonPrice(Villa $villa)
    {
        return $this->collection($villa->seasonPrice, new SeasonPriceTransformer) ?? null;
    }

    /**
     * Include recommendation
     *
     * @param Villa $villa
     * @return \League\Fractal\Resource\Collection
     */
    public function includeRecommendation(Villa $villa)
    {
        return $this->collection($villa->recommendation, new RecommendationTransformer) ?? null;
    }

    /**
     * Include image
     *
     * @param Villa $villa
     * @return \League\Fractal\Resource\Collection
     */
    public function includeImage(Villa $villa)
    {
        return $this->collection($villa->image, new ImageTransformer)?? null;
    }

    /**
     * Include video
     *
     * @param Villa $villa
     * @return \League\Fractal\Resource\Collection
     */
    public function includeVideos(Villa $villa)
    {
        return $this->collection($villa->videos, new VideoTransformer) ?? null;
    }

    /**
     * Include testimonial
     *
     * @param Villa $villa
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTestimonial(Villa $villa)
    {
        return $this->collection($villa->testimonial, new TestimonialTransformer) ?? null;
    }

    /**
     * Include calendar
     *
     * @param Villa $villa
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCalendar(Villa $villa)
    {
        return $this->collection($villa->calendar, (new CalendarTransformer)) ?? null;
    }

    /**
     * Include review
     *
     * @param Villa $villa
     * @return \League\Fractal\Resource\Collection
     */
    public function includeReview(Villa $villa)
    {
        return $this->collection($villa->review, new ReviewTransformer) ?? null;
    }

    /**
     * Include Amenities
     *
     * @param Villa $villa
     * @return \League\Fractal\Resource\Item
     */
    public function includeAmenities(Villa $villa)
    {
        return $this->item($villa, new AmenitiesTransformer) ?? null;
    }

    /**
     * Include Agent
     *
     * @param Villa $villa
     * @return \League\Fractal\Resource\Item
     */
    public function includeAgent(Villa $villa)
    {
        return $this->item($villa->agent, new AgentTransformer) ?? null;
    }

    /**
     * Include Rates
     *
     * @param Villa $villa
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeRates(Villa $villa)
    {
        return $this->collection($villa->rates, new RatesTransformer) ?? null;
    }

    /**
     * Include Location
     *
     *
     * @param Villa $villa
     * @return \League\Fractal\Resource\Collection|null
     */
    public function includeLocation(Villa $villa)
    {
        return $this->collection($villa->villaLocation, new VillaLocationTransformer()) ?? null;
    }
}