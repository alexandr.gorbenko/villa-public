<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 3/25/19
 * Time: 12:15 PM
 */

namespace App\Http\Transformers;

use App\Models\Villa\Vm\Location;

class VillaLocationTransformer extends BaseTransformer
{

    /**
     * Transform a response with a transformer.
     *
     * @param Location $location
     * @return array
     */
    public function transform(Location $location)
    {
        return [
            'villa_id' => $location->villa_id,
            'vg_number' => $location->vg_number,
            'villa_name' => $location->villa_name,
            'destination_name' => $location->destination_name,
            'destination_id' => $location->destination_id,
            'location_name' => $location->location_name,
            'location_id' => $location->location_id,
            'region_name' => $location->region_name,
            'region_id' => $location->region_id,
            'rating' => $location->rating,
            'baths' => $location->baths,
            'sleeps' => $location->sleeps,
            'beds' => $location->beds,
            'reviews' => $location->reviews,
            'currency' => $location->currency,
            'low' => $location->low,
            'high' => $location->high,
            'agent_id' => $location->agent_id,
            'agent_name' => $location->agent_name,
        ];

}