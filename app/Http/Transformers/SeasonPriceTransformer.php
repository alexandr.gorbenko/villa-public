<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/12/19
 * Time: 8:26 PM
 */

namespace App\Http\Transformers;

use App\Models\Villa\SeasonPrice;

class SeasonPriceTransformer extends BaseTransformer
{
    /**
     * Transform a response with a transformer.
     *
     * @param SeasonPrice $seasonPrice
     * @return array
     */
    public function transform(SeasonPrice $seasonPrice)
    {
        return [
            'villa_season_price_id' => $seasonPrice->villa_season_price_id,
            'villa_id' => (int) $seasonPrice->villa_id,
            'season_type_id' => (int) $seasonPrice->season_type_id,
            'price' => $seasonPrice->price,
            'nr_of_rooms' => $seasonPrice->nr_of_rooms,
            'updated_at' => $seasonPrice->updated_at,
            'min_nr_days' => $seasonPrice->min_nr_days,
            'master_included' => $seasonPrice->master_included,
        ];
    }

}