<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/25/19
 * Time: 11:44 AM
 */

namespace App\Http\Transformers;

use App\Models\Villa\Villa;

class AmenitiesTransformer extends BaseTransformer
{


    /**
     * Transform a response with a transformer.
     *
     * @param Villa $villa
     * @return array
     */
    public function transform(Villa $villa)
    {
        return [
            'villa_id' => (int) $villa->villa_id,
            'oceanview' => $villa->oceanview,
            'pool' => $villa->pool,
            'ac' => $villa->ac,
            'maid' => $villa->maid,
            'chef' => $villa->chef,
            'broadband' => $villa->broadband,
            'tennis' => $villa->tennis,
            'daily_breakfast' => $villa->daily_breakfast,
            'car_and_driver' => $villa->car_and_driver,
        ];
    }

}