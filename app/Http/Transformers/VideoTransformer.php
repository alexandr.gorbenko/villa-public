<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/12/19
 * Time: 8:44 PM
 */

namespace App\Http\Transformers;

use App\Models\Video;

class VideoTransformer extends BaseTransformer
{
    /**
     * Transform a response with a transformer.
     *
     * @param Video $video
     * @return array
     */
    public function transform(Video $video)
    {
        return [
            'video_id' => $video->video_id,
            'http_code' => $video->http_code,
            'villa_id' => (int) $video->villa_id,
            'created_at' => $video->created_at,
            'updated_at' => $video->updated_at,
        ];
    }

}