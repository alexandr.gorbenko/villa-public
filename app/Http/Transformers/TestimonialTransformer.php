<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/12/19
 * Time: 8:47 PM
 */

namespace App\Http\Transformers;

use App\Models\Testimonial;

class TestimonialTransformer extends BaseTransformer
{
    /**
     * Transform a response with a transformer.
     *
     * @param Testimonial $testimonial
     * @return array
     */
    public function transform(Testimonial $testimonial)
    {
        return [
            'testimonial_id' => $testimonial->testimonial_id,
            'name' => $testimonial->name,
            'detail' => $testimonial->detail,
            'villa_id' => (int) $testimonial->villa_id,
        ];
    }

}