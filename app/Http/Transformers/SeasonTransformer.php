<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/12/19
 * Time: 8:21 PM
 */

namespace App\Http\Transformers;

use App\Models\Season\Season;

class SeasonTransformer extends BaseTransformer
{
    /**
     * Transform a response with a transformer.
     *
     * @param Season $season
     * @return array
     */
    public function transform(Season $season)
    {
        return [
            'season_id' => $season->season_id,
            'season_type_id' => (int) $season->season_type_id,
            'name' => $season->name,
            'from_date' => $season->from_date,
            'to_date' => $season->to_date,
            'is_cyclic' => $season->is_cyclic,
            'updated_at' => $season->updated_at,
            'default_type' => $season->default_type,
        ];
    }
}