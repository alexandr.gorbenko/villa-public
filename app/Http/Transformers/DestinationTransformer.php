<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/12/19
 * Time: 9:34 PM
 */

namespace App\Http\Transformers;

use App\Models\Destination;

class DestinationTransformer extends BaseTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'location'
    ];

    /**
     * Transform a response with a transformer.
     *
     * @param Destination $destination
     * @return array
     */
    public function transform(Destination $destination)
    {
        return [
            'destination_url' => $destination->destination_url,
            'display_online' => $destination->display_online,
            'destination_id' => $destination->destination_id,
            'name' => $destination->name,
            'title' => $destination->title,
            'description' => $destination->description,
            'long_description' => $destination->long_description,
            'position' => $destination->position,
            'agent_id' => (int) $destination->agent_id,
            'transfer_info' => $destination->transfer_info,
            'local_contact' => $destination->local_contact,
            'activities' => $destination->activities,
            'head_title' => $destination->head_title,
            'head_code' => $destination->head_code,
            'meta_description' => $destination->meta_description,
            'currency_id' => (int) $destination->currency_id,
            'image' => $destination->image,
            'general_guide' => $destination->general_guide,
            'slug' => $destination->slug,
        ];
    }

    /**
     * Include location
     *
     * @param Destination $destination
     * @return \League\Fractal\Resource\Collection
     */
    public function includeLocation(Destination $destination)
    {
        return $this->collection($destination->location, new LocationTransformer());
    }
}