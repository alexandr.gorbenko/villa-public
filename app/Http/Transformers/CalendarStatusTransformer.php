<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/19/19
 * Time: 12:32 PM
 */

namespace App\Http\Transformers;

use App\Models\Calendar\Status;

class CalendarStatusTransformer extends BaseTransformer
{
    /**
     * Transform a response with a transformer.
     *
     * @param Status $status
     * @return array
     */
    public function transform(Status $status)
    {
        return [
            'calendar_status_id' => $status->calendar_status_id,
            'name' => $status->name,
            'active' => $status->active,
            'is_disable' => $status->is_disable,
        ];
    }

}