<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/12/19
 * Time: 8:51 PM
 */

namespace App\Http\Transformers;

use App\Models\Calendar\Calendar;

class CalendarTransformer extends BaseTransformer
{
    /**
     * List of resources default include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'status'
    ];

    /**
     * Transform a response with a transformer.
     *
     * @param Calendar $calendar
     * @return array
     */
    public function transform(Calendar $calendar)
    {
        return [
            'calendar_id' => $calendar->calendar_id,
            'villa_id' => (int) $calendar->villa_id,
            'from_date' => $calendar->from_date,
            'to_date' => $calendar->to_date,
            'late_checkout' => $calendar->late_checkout,
            'client_name' => $calendar->client_name,
            'notes' => $calendar->notes,
            'calendar_status_id' => $calendar->calendar_status_id,
            'notifications_email_address' => $calendar->notifications_email_address,
            'hold_for' => $calendar->hold_for,
            'expired_at' => $calendar->expired_at,
            'external_id' => (int) $calendar->external_id,
            'external_source' => $calendar->external_source,
            'created_at' => $calendar->created_at,
            'updated_at' => $calendar->updated_at,
            'update_by' => $calendar->update_by,
            'status_id' => (int) $calendar->status_id,
            'valid_by' => $calendar->valid_by,
            'created_by' => $calendar->created_by,
        ];
    }

    /**
     * Include review
     *
     * @param Calendar $calendar
     * @return \League\Fractal\Resource\Item
     */
    public function includeStatus(Calendar $calendar)
    {
        return $this->item($calendar->status, new CalendarStatusTransformer);
    }

}