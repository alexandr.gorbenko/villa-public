<?php

namespace App\Http\Transformers;


use App\Models\Villa\VillaList;

class VillaListTransformer extends BaseTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'villa'
    ];

    public function transform(VillaList $villaList) {
        return [
            'villa_list_id' => $villaList->villa_list_id,
            'image' => $villaList->image,
            'description' => $villaList->description,
            'head_title' => $villaList->head_title,
            'head_code' => $villaList->head_code,
            'meta_description' => $villaList->meta_description,
            'created_at' => $villaList->created_at,
            'updated_at' => $villaList->updated_at,
            'slug' => $villaList->slug,
        ];
    }

    /**
     * Include Villa
     *
     * @param VillaList $villaList
     * @return \League\Fractal\Resource\Collection
     */
    public function includeVilla(VillaList $villaList)
    {
        return $this->collection($villaList->villa, new VillaTransformer);
    }

}