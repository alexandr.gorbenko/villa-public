<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/12/19
 * Time: 8:34 PM
 */

namespace App\Http\Transformers;

use App\Models\Villa\Recommendation;

class RecommendationTransformer extends BaseTransformer
{
    /**
     * Transform a response with a transformer.
     *
     * @param Recommendation $recommendation
     * @return array
     */
    public function transform(Recommendation $recommendation)
    {
        return [
            'ecs_villa_id' => (int) $recommendation->ecs_villa_id,
            'ecs_id' => (int) $recommendation->ecs_id,
            'villa_id' => (int) $recommendation->villa_id,
            'highly_recommended' => $recommendation->highly_recommended,
            'position' => $recommendation->position,
            'season' => $recommendation->season,
            'beds' => $recommendation->beds,
            'period' => $recommendation->period,
            'price' => $recommendation->price,
            'discount' => $recommendation->discount,
            'arrive_at' => $recommendation->arrive_at,
            'depart_at' => $recommendation->depart_at,
            'custome_mig' => $recommendation->custome_mig,
            'seasons_mig' => $recommendation->seasons_mig,
            'prices_mig' => $recommendation->prices_mig,
        ];
    }

}