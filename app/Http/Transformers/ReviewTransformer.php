<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/12/19
 * Time: 8:54 PM
 */

namespace App\Http\Transformers;

use App\Models\Review;

class ReviewTransformer extends BaseTransformer
{
    /**
     * Transform a response with a transformer.
     *
     * @param Review $review
     * @return array
     */
    public function transform(Review $review)
    {
        return [
            'review_id' => $review->review_id,
            'villa_id' => (int) $review->villa_id,
            'name' => $review->name,
            'title' => $review->title,
            'description' => $review->description,
            'date_stayed' => $review->date_stayed,
            'rating' => $review->rating,
            'created_at' => $review->created_at,
            'updated_at' => $review->updated_at,
            'date_' => $review->date_,
        ];
    }

}