<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 3/2/19
 * Time: 4:18 PM
 */
namespace App\Classes;

use Carbon\Carbon;

/**
 * Class BaseClass
 * @package App\Classes
 *
 */
abstract class BaseClass implements BaseInterface
{
    /**
     * @param string $intName
     * @param int $value
     * @return object
     */
    public function setInt(string $intName, int $value = null)
    {
        $this->{lcfirst($intName)} = $value;
        return $this;
    }

    /**
     * @param string $boolName
     * @param string $value
     * @return object
     */
    public function setString(string $boolName, string $value = null)
    {
        $this->{lcfirst($boolName)} = $value;
        return $this;
    }

    /**
     * @param string $dateName
     * @param string $value
     * @return object
     */
    public function setDate(string $dateName, string $value = null)
    {
        if($value) $this->{lcfirst($dateName)} = Carbon::parse($value);
        return $this;
    }

    /**
     * @param string $attributeName
     * @return bool
     */
    public function isSetAttribute(string $attributeName):bool
    {
        return (isset($this->{lcfirst($attributeName)}));
    }

    /**
     * Magic getter
     *
     * @param string $name
     * @param array $arguments
     * @return null
     */
    public function __call(string $name, array $arguments)
    {
        if (preg_match('/^get/i', $name)) {
            return $this->{lcfirst(preg_replace('/^get/i', '', $name))} ?? null;
        }
    }
}