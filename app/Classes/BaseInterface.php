<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 3/2/19
 * Time: 4:19 PM
 */
namespace App\Classes;

interface BaseInterface
{
    /**
     * @param string $intName
     * @param int $value
     * @return BaseInterface
     */
    public function setInt(string $intName, int $value = null);

    /**
     * @param string $boolName
     * @param string $value
     * @return BaseInterface
     */
    public function setString(string $boolName, string $value);

    /**
     * @param string $dateName
     * @param string $value
     * @return BaseInterface
     */
    public function setDate(string $dateName, string $value = null);

    /**
     * @param string $attributeName
     * @return bool
     */
    public function isSetAttribute(string $attributeName):bool;

}