<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 2/12/19
 * Time: 10:13 PM
 */

namespace App\Classes\Villa;

use Carbon\Carbon;
use App\Classes\BaseClass;

/**
 * Class Filter
 * @package App\Classes\Villa
 *
 * @method Carbon   getCheckIn()
 * @method Carbon   getCheckOut()
 * @method int      getDestinationId()
 * @method int      getLocationId()
 * @method int      getPriceFrom()
 * @method int      getPriceTo()
 * @method string   getPriceOrderBy()
 * @method string   getVgNumber()
 */
class Filter extends BaseClass
{
    /**
     * @var Carbon
     */
    protected $checkIn;

    /**
     * @var Carbon
     */
    protected $checkOut;

    /**
     * @var int
     */
    protected $destinationId;

    /**
     * @var int
     */
    protected $locationId;

    /**
     * @var int
     */
    protected $priceFrom;

    /**
     * @var int
     */
    protected $priceTo;

    /**
     * @var string
     */
    protected $priceOrderBy;

    /**
     * @var string
     */
    protected $vgNumber;
}