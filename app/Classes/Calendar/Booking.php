<?php
/**
 * Created by PhpStorm.
 * User: pvavilov
 * Date: 3/2/19
 * Time: 4:17 PM
 */

namespace App\Classes\Calendar;

use Carbon\Carbon;
use App\Classes\BaseClass;

/**
 * Class Booking
 * @package App\Classes\Calendar
 *
 * @method Carbon   getVillaId()
 * @method Carbon   getCheckIn()
 * @method Carbon   getCheckOut()
 * @method string   getClientName()
 * @method string   getNotificationsEmailAddress()
 */
class Booking extends BaseClass
{
    /**
     * @var int
     */
    protected $villaId;
    /**
     * @var Carbon
     */
    protected $checkIn;

    /**
     * @var Carbon
     */
    protected $checkOut;

    /**
     * @var string
     */
    protected $clientName;

    /**
     * @var string
     */
    protected $notificationsEmailAddress;

}