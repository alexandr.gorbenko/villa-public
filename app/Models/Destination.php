<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    protected $table = 'DESTINATION';

    protected $primaryKey = 'destination_id';

    public function location()
    {
        return $this->hasMany(
            'App\Models\Location',
            'destination_id',
            'destination_id'
        );
    }
}
