<?php

namespace App\Models\Calendar;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $table = 'CALENDAR';

    protected $primaryKey = 'calendar_id';

    protected $fillable = [
        'villa_id',
        'from_date',
        'to_date',
        'client_name',
        'notifications_email_address'
    ];

    public function status()
    {
        return $this->hasOne(
            'App\Models\Calendar\Status',
            'calendar_status_id',
            'calendar_status_id'
        );
    }
}
