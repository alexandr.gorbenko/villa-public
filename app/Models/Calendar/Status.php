<?php

namespace App\Models\Calendar;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'CALENDAR_STATUS';

    protected $primaryKey = 'calendar_status_id';
}
