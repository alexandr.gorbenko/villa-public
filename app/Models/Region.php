<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'REGION';

    protected $primaryKey = 'region_id';


    public function destination()
    {
        return $this->hasOne(
            'App\Models\Destination',
            'destination_id',
            'destination_id'
        );
    }

    public function location()
    {
        return $this->hasOne(
            'App\Models\Location',
            'location_id',
            'location_id'
        );
    }
}
