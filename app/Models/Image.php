<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'IMAGE';

    protected $primaryKey = 'image_id';
}
