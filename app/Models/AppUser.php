<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppUser extends Model
{
    protected $table = 'APP_USER';

    protected $primaryKey = 'user_id';
}
