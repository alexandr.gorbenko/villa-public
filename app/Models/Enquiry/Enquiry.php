<?php

namespace App\Models\Enquiry;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $table = 'ENQUIRY';

    protected $primaryKey = 'enquiry_id';
}
