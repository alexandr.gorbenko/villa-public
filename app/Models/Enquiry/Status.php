<?php

namespace App\Models\Enquiry;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'ENQUIRY_STATUS';

    protected $primaryKey = 'enquiry_status_id';
}
