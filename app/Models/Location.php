<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'LOCATION';

    protected $primaryKey = 'location_id';

    public function destination()
    {
        return $this->hasOne(
            'App\Models\Destination',
            'destination_id',
            'destination_id'
        );
    }
}
