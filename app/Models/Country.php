<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'COUNTRY';

    protected $primaryKey = 'country_id';
}
