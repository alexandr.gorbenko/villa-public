<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $table = 'AGENT';

    protected $primaryKey = 'agent_id';
}
