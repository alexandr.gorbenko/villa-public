<?php

namespace App\Models\Villa;

use Illuminate\Database\Eloquent\Model;

class Recommendation extends Model
{
    protected $table = 'VILLA_RECOMMENDATION';
}
