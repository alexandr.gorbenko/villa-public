<?php

namespace App\Models\Villa;

use Illuminate\Database\Eloquent\Model;

class Villa extends Model
{
    protected $table = 'VILLA';

    protected $primaryKey = 'villa_id';

    public function villaList()
    {
        return $this->belongsToMany(
            'App\Models\Villa\VillaList',
            'VILLA_LIST_VILLA',
            'villa_id',
            'villa_list_id'
        );
    }

    public function season()
    {
        return $this->belongsToMany(
            'App\Models\Season\Season',
            'VILLA_SEASON',
            'villa_id',
            'season_id'
        );
    }

    public function seasonPrice()
    {
        return $this->hasMany(
            'App\Models\Villa\SeasonPrice',
            'villa_id',
            'villa_id'
        );
    }

    public function recommendation()
    {
        return $this->hasMany(
            'App\Models\Villa\Recommendation',
            'villa_id',
            'villa_id'
        );
    }

    public function videos()
    {
        return $this->hasMany(
            'App\Models\Video',
            'villa_id',
            'villa_id'
        );
    }

    public function image()
    {
        return $this->hasMany(
            'App\Models\Image',
            'villa_id',
            'villa_id'
        );
    }

    public function testimonial()
    {
        return $this->hasMany(
            'App\Models\Testimonial',
            'villa_id',
            'villa_id'
        );
    }

    public function calendar()
    {
        return $this->hasMany(
            'App\Models\Calendar\Calendar',
            'villa_id',
            'villa_id'
        );
    }

    public function enquiry()
    {
        return $this->hasMany(
            'App\Models\Enquiry\Enquiry',
            'villa_id',
            'villa_id'
        );
    }

    public function review()
    {
        return $this->hasMany(
            'App\Models\Review',
            'villa_id',
            'villa_id'
        );
    }

    public function agent()
    {
        return $this->hasOne(
            'App\Models\Agent',
            'agent_id',
            'agent_id'
        );
    }

    public function rates()
    {
        return $this->hasMany(
            'App\Models\Villa\Vm\Rates',
            'villa_id',
            'villa_id'
        );
    }

    public function villaLocation()
    {
        return $this->hasMany(
            'App\Models\Villa\Vm\Location',
            'villa_id',
            'villa_id'
        );
    }
}
