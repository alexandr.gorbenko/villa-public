<?php

namespace App\Models\Villa;

use Illuminate\Database\Eloquent\Model;

class VillaList extends Model
{
    protected $table = 'VILLA_LIST';

    protected $primaryKey = 'villa_list_id';

    public function villa()
    {
        return $this->belongsToMany(
            'App\Models\Villa\Villa',
            'VILLA_LIST_VILLA',
            'villa_list_id',
            'villa_id'
        );
    }
}
