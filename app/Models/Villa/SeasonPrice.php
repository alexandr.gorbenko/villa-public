<?php

namespace App\Models\Villa;

use Illuminate\Database\Eloquent\Model;

class SeasonPrice extends Model
{
    protected $table = 'VILLA_SEASON_PRICE';

    protected $primaryKey = 'villa_season_price_id';

    public function type()
    {
        return $this->belongsTo(
            'App\Models\Season\Type',
            'season_type_id',
            'season_type_id'
        );
    }


    public function scopeFilter($query, string $fromDate, string $toDate)
    {
        return $query->where('from_date', '<=', $fromDate)
            ->where('to_date', '>=', $toDate);
    }
}
