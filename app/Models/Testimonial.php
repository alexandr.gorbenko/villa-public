<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $table = 'TESTIMONIAL';

    protected $primaryKey = 'testimonial_id';
}
