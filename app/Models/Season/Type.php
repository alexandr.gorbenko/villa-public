<?php

namespace App\Models\Season;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'SEASON_TYPE';

    protected $primaryKey = 'season_type_id';
}
