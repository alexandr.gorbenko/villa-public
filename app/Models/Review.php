<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'REVIEW';

    protected $primaryKey = 'review_id';
}
