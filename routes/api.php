<?php

//use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


$api = app('Dingo\Api\Routing\Router');


$api->version('v1', ['prefix' => 'api/v1', 'namespace' => 'App\Http\Controllers\Api'], function ($api) {
    // Villa
    $api->get('/villa', 'VillaController@all')->name('villa_all');
    $api->get('/villa/filter', 'VillaController@filter')->name('villa_filter');
    $api->get('/villa/{id}', 'VillaController@item')->where('id', '[0-9]+')->name('villa_item');

    // VillaList
    $api->get('/villa/list', 'VillaListController@all')->name('villaList_all');
    $api->get('/villa/list/{id}', 'VillaListController@item')->where('id', '[0-9]+')->name('villaList_item');

    // Location
    $api->get('/location', 'LocationController@location')->name('location');

    // Destination
    $api->get('/destination', 'DestinationController@destination')->name('destination');

    // Region
    $api->get('/region', 'RegionController@region')->name('region');

    // Contact
    $api->post('/contact', 'ContactController@contact')->name('contact');

    // Booking
    $api->post('/booking', 'BookingController@booking')->name('booking');

    // Newsletter
    $api->post('/newsletter', 'NewsletterController@newsletter')->name('newsletter');

    // Agent
    $api->get('/agent', 'AgentController@all')->name('agent_all');
    $api->get('/agent/{id}', 'AgentController@item')->where('id', '[0-9]+')->name('agent_item');
});